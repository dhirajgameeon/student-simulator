using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlowManager : MonoBehaviour
{
    public bool isMissed = false;
    public bool TaskFailed = false;
    public bool TaskPassed = false;
    public List<StudentController> Character = new List<StudentController>();

    public Slider ProgressBar;
    public float upSpeed = 10;
    private float currentProgress;


    public GameObject F_Emoji;
    public GameObject F_UI;

    public GameObject P_Emoji;
    public GameObject P_UI;

    public TeacherController TC;


    public void AfterMissedBall()
    {
/*        foreach(Animator anim in Character)
        {
            anim.Play("LookBehind");
        }*/
    }


    private void Update()
    {
        progress();
        FailedCondition();
        PassCondition();
    }
    void progress()
    {
        if (currentProgress < TC.HitRegester)
        {
            currentProgress += upSpeed * Time.deltaTime;
            if (currentProgress >=  TC.HitRegester)
                currentProgress =  TC.HitRegester;
        }
        ProgressBar.value = currentProgress;
    }

    void FailedCondition()
    {
        if (TaskFailed)
        {
            EvenManager.TriggerSFXOneShotPlayEvent(AudioID.GameLoose);
            F_Emoji.GetComponent<ParticleSystem>().Play();
            F_UI.SetActive(true);
            TaskFailed = false;
        }
    }
    void PassCondition()
    {
        if (TaskPassed)
        {
            EvenManager.TriggerSFXOneShotPlayEvent(AudioID.GameWin);
            P_Emoji.GetComponent<ParticleSystem>().Play();
            P_UI.SetActive(true);
            TaskPassed = false;
        }
    }
    public void Emoji()
    {
        /*EvenManager.TriggerSFXOneShotPlayEvent(AudioID.ChildrenLaughing);*/
        Character[0].Emoji();
        Character[1].Emoji();
        Character[0].animator.Play("Clapping");
        Character[1].animator.Play("Clapping");
    }

}
