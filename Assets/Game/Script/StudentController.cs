using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StudentController : MonoBehaviour
{
    public FlowManager flowManager;
    public int lookingSide;
    public Animator animator;
    public ParticleSystem HappyEmoji;
    public FaceExpression FE;
    void Update()
    {
        lookBehind();
    }

    bool isLooked = false;
    public void lookBehind()
    {
        if (flowManager.isMissed && !isLooked)
        {
            EvenManager.TriggerSFXOneShotPlayEvent(AudioID.ChildrenLaughing);
            animator.Play("Look Behind " + lookingSide);
            FE.FaceType = Face.Idle;
            isLooked = true;
        }
    }
    public void Emoji()
    {
        HappyEmoji.Play();
    }
}
