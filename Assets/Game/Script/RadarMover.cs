using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadarMover : MonoBehaviour
{

    public Camera cam;
    public bool isFoodWar = false;
    public FW_FoodThrow foodThrow;
    void Update()
    {
        
        if(!isFoodWar) checkReycast();
        if(isFoodWar) checkReycastFW();
    }
    void checkReycast()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        if(Physics.Raycast(ray, out RaycastHit hit,100))
        {
            transform.position = hit.point;            
        }
    }
    void checkReycastFW()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        if (foodThrow.BallRigidbody && Physics.Raycast(ray, out RaycastHit hit, 100))
        {
            transform.position = hit.point;
        }
    }
}
