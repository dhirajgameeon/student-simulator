using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class TeacherController : MonoBehaviour
{
    public Animator anim;
    public Transform OutPosition;
    public int HitCount;
    public int HitRegester;
    public GameObject Tear;
    public GameObject SadEmoji;
    public GameObject laserEye;
    public GameObject FOV;

    public Transform PlayerPosition;
    public FaceExpression FE;

    public FlowManager fm;
    public NavMeshAgent agent;
    private float turnSmoothVelocity;
    public float rotationSmooth;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        move();        
    }


    public void move()
    {
        if (agent.velocity.magnitude > 0f)
        {
            float targetAngle = Mathf.Atan2(agent.velocity.x, agent.velocity.z) * Mathf.Rad2Deg;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, rotationSmooth);
            transform.rotation = Quaternion.Euler(0, angle, 0);            
        }
        anim.SetFloat("speed", agent.velocity.magnitude);
    }
    public void Rotate(float direction, float time)
    {
        LeanTween.move(this.gameObject, OutPosition.position, 4);
        LeanTween.rotate(this.gameObject, new Vector3(0, direction, 0), time);
        Invoke("SadEmojiActive", 0.1f);
    }
    public void RotateDirection(float direction, float time)
    {
        LeanTween.rotate(this.gameObject, new Vector3(0, direction, 0), time);
    }

    public void SadEmojiActive()
    {
        Tear.SetActive(true);
        SadEmoji.SetActive(true);

        laserEye.SetActive(false);
        FOV.SetActive(false);
    }

    public void hitTrigger()
    {

        anim.SetTrigger("hit");
        RotateDirection(180, 0.35f);
        FE.FaceType = Face.Angry;
        LeanTween.delayedCall(3.25f, () => {
            anim.ResetTrigger("hit");
            RotateDirection(0, 0.1f);
        });
    }

    public void LookDirection(int i)
    {
        anim.Play("sad");
        RotateDirection(180, 0.35f);
        LeanTween.delayedCall(0.31f, () => {
            anim.Play("Looking " + i);
        });
        FE.FaceType = Face.Angry;
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("PaperBall"))
        {
            //gothit
            hitTrigger();
            HitCount++;
            if(HitCount < 3) laserEye.SetActive(true);
        }

        if (HitCount >= 3)
        {
            RotateDirection(180, 0.35f);
            EvenManager.TriggerSFXOneShotPlayEvent(AudioID.TeacherAngerReaction);
            anim.SetBool("angry", true);
        }

        if (other.gameObject.CompareTag("StudentStop"))
        {
            agent.isStopped = true;            
            anim.SetTrigger("go out");
        }
    }
}
