using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeacherAnimationController : MonoBehaviour
{
    public TeacherController TC;

    public void Spawnl()
    {
        TC.laserEye.SetActive(false);
        GameManager.instance.SpawnBall();
        TC.HitRegester = TC.HitCount;
    }

    public void SFXMOVEOUT()
    {
        EvenManager.TriggerSFXOneShotPlayEvent(AudioID.TeacherCatchesStudent);
    }

    public void MoveOut()
    {
        EvenManager.TriggerSFXOneShotPlayEvent(AudioID.TeacherSadOut);
        gameObject.GetComponentInParent<TeacherController>().Rotate(90, 0.15f);
        LeanTween.delayedCall(0.55f, () => { FindObjectOfType<FlowManager>().TaskPassed = true; });
        FindObjectOfType<FlowManager>().Emoji();

        TC.HitRegester = TC.HitCount;
    }   
    public void TaskCompleted()
    {
        FindObjectOfType<FlowManager>().TaskFailed = true;
    }
}
