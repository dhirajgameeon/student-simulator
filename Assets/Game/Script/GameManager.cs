using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;


    public bool isThrownBall;
    public bool isGameStart;
    public bool isMousePressed;
    public Transform _ProjectileRacePoint;
    public Transform shootPoint;
    public _PlayerProjectile playerProjectile;

    public Rigidbody Ball;
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        EvenManager.TriggerSFXOneShotPlayEvent(AudioID.BGM);
        /* GameObject ball = Instantiate(Ball, shootPoint.position, Quaternion.identity).gameObject;
         playerProjectile.BallRigidbody = ball.GetComponent<Rigidbody>();*/
    }

    // Update is called once per frame
    void Update()
    {
        if (isGameStart && Input.GetMouseButton(0))
        {
            isMousePressed = true;
        }
        if(isGameStart && Input.GetMouseButtonUp(0))
        {
            isMousePressed = false;
        }



        if (Input.GetKeyDown(KeyCode.S))
        {
            SpawnBall();
        }
    }

    public void StartGame()
    {
        LeanTween.delayedCall(0.15f, () => { isGameStart = true; }); 
    }
    public void SpawnBall()
    {
        GameObject ball = Instantiate(Ball, shootPoint.position, Quaternion.identity).gameObject;
        playerProjectile.BallRigidbody = ball.GetComponent<Rigidbody>();
        EvenManager.TriggerSFXOneShotPlayEvent(AudioID.PaperSpwan);
        if (isThrownBall) isThrownBall = false;
        //ball.GetComponent<Ball>().shootPoint = shootPoint;
    }
}
