using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DemoPlayAudio : MonoBehaviour
{
    void Start()
    {
        BGMplay();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.B)) PlayOnShot();
    }

    void BGMplay()
    {
        EvenManager.TriggerBGMPlayEvent(AudioID.BGM);
    }
    void PlayOnShot()
    {
        EvenManager.TriggerSFXOneShotPlayEvent(AudioID.BGM);
    }
}
