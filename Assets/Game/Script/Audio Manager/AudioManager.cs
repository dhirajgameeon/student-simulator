using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Xml;
using System.Linq;
using System;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

public enum AudioID{
    //----------------PaperThrow----------------//
    BGM = 0,
    ChildrenLaughing = 1,
    GameLoose = 2,
    GameWin = 3,
    PaperHitObject = 4,
    PaperHitTeacher = 5,
    PaperSpwan = 6,
    PaperSwoosh = 7,
    TeacherAngerReaction = 8,
    TeacherCatchesStudent = 9,
    TeacherMissed = 10,
    TeacherSadOut = 11,
    //----------------Cafeteria----------------//
    Wrench = 12,
    WrenchLoop = 13,
    ScrewCompleted= 14,
    FemaleScream = 15,
    MaleScream = 16,
    PlateOnTable = 17,
    TabelFall = 18,
    StudentHumming = 19,
    Invalid
}
public class AudioManager : Singleton<AudioManager>
{
    public List<AudioClip> AudioType = new List<AudioClip>();
    public AudioSource SFXAudioSource;
    public AudioSource SFXOnShotAudioSource;
    public AudioSource BGMAudioSource;

    public bool IsBGMOn, IsSFXOn, _IsPlaying = true;
    public float DefaultSFXVolume, DefaultBGMVolume, BGMFadeDuration = 1f;

    public AudioID CurrentMusicID = AudioID.Invalid;
    private float BGMVolume { get { return IsBGMOn ? DefaultBGMVolume : 0f; } }

    private void Awake()
    {
        EvenManager.AddBGMPlayEvent(OnMusicPlayEvent);
    }
    public void Start()
    {
        _IsPlaying = false;


        EvenManager.AddSFXOneShotPlayEvent(OnSFXOneShotPlay);

        SFXAudioSource.loop = false;
        SFXOnShotAudioSource.loop = false;
        BGMAudioSource.loop = true;
    }

    private void OnDestroy()
    {
        EvenManager.RemoveBGMPlayEvent(OnMusicPlayEvent);
        EvenManager.RemoveSFXOneShotPlayEvent(OnSFXOneShotPlay);
    }

    void OnSFXOneShotPlay(AudioID ID, UnityAction act)
    {
        PlayOneShotSFX(ID, DefaultSFXVolume);
        act?.Invoke();
    }
    void PlayOneShotSFX(AudioID id, float volumeScale)
    {
        if (!IsSFXOn) return;
        if(AudioID.BGM == id)
        {
            print("Id?");
            SFXAudioSource.Stop();
            SFXAudioSource.clip = AudioType[(int)id];
            SFXAudioSource.Play();
            return;
        }
        SFXOnShotAudioSource.PlayOneShot(AudioType[(int)id], volumeScale);
    }

    void OnMusicPlayEvent(AudioID id)
    {
        if (id == CurrentMusicID) return;
        Debug.Log("id" + id);
        switch (id)
        {
            case AudioID.BGM: DefaultBGMVolume = 0.1f; break;
            default: DefaultBGMVolume = 0.1f; break;
        }

        if (CurrentMusicID != AudioID.Invalid)
            StartCoroutine(FadeOutMusic(() => { FadeInMusic(id); }));
        else
            FadeInMusic(id);
    }

    IEnumerator FadeOutMusic(System.Action onFadeCompleteAction = null)
    {
        print("BGM Stopped");
        yield return StartCoroutine(ChangeVolume(BGMAudioSource, BGMAudioSource.volume, 0f));

        BGMAudioSource.Stop();
        CurrentMusicID = AudioID.Invalid;
        BGMAudioSource.clip = null;
        
        if (onFadeCompleteAction != null) onFadeCompleteAction.Invoke();
    }
    void FadeInMusic(AudioID musicId)
    {
        CurrentMusicID = musicId;
        BGMAudioSource.clip = AudioType[(int)musicId];
        BGMAudioSource.Play();
        StartCoroutine(ChangeVolume(BGMAudioSource, 0f, BGMVolume));
    }

    IEnumerator ChangeVolume(AudioSource source, float startVol, float endVol)
    {
        float t = 0f, dur = BGMFadeDuration;

        while (t < dur)
        {
            source.volume = Mathf.Lerp(startVol, endVol, t / dur);
            t += Time.deltaTime;
            yield return null;
        }

        source.volume = endVol;
    }
}
