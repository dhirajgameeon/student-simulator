using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wind : MonoBehaviour
{
    public float time;
    public bool isMove = false;
    public ParticleSystem wind;
    public void moveWind()
    {
        isMove = true;
        //LeanTween.move(gameObject, transform.forward, time);
    }

    private void Update()
    {
        if (isMove)
        {
            transform.Translate(transform.right * time * Time.deltaTime);
            wind.Play();
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("PaperBall"))
        {
            isMove=false;
        }
    }
}
