using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FW_PlayerAnimation : MonoBehaviour
{
    public FW_Player FWP;
    public void CameraChange()
    {
        FW_CinemachineCamera.instance.switchCamera("Player Target");
    }

    public void ShowTray()
    {
        FWP.TrayInHand.SetActive(false);
        FWP.TrayOnTable.SetActive(true);

    }

    public void ActivateTargets()
    {
        FW_FlowManager.instance.isPlayerSeatOnBench = true;
    }













    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
