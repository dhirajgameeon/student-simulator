using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class FW_FlowManager : MonoBehaviour
{
    public static FW_FlowManager instance;
    public List<FW_ChirCharacterSelector> TargetCharacters = new List<FW_ChirCharacterSelector>();
    public List<FW_StundetThrowFoodEO> Students = new List<FW_StundetThrowFoodEO>();

    public Animator PerfectTextUI;
    [Space(5)]
    public Slider ProgressBar;
    public float upSpeed = 10;
    private float currentProgress;
    [Space(5)]
    public bool isFoodCollected = false;
    public bool isComplete = false;
    public bool isGameOver = false;
    public bool isPlayerSeatOnBench = false;
    public int HitPlayers = 0;
    public float delayAfterFoodCallection = 2.5f;
    public float delayOnCompleteTask = 4;
    public float failConditionDelay = 1.5f;
    public UnityEvent FoodCollected, Perfect, Failed;
    public UnityEvent FoodCollectionStop;

    void Start()
    {
        instance = this;
        ProgressBar.gameObject.SetActive(false);
    }


    void Update()
    {
        SwitchCamera();
        checkForTargets();
        GameOver();
        progress();
    }

    void progress()
    {
        if (currentProgress < HitPlayers)
        {
            currentProgress += upSpeed * Time.deltaTime;
            if (currentProgress >= HitPlayers)
                currentProgress = HitPlayers;
        }
        ProgressBar.value = currentProgress;
    }

    void SwitchCamera()
    {
        if (isFoodCollected && !isComplete)
        {
            FoodCollected.Invoke();
            LeanTween.delayedCall(delayAfterFoodCallection, () => {
                FoodCollectionStop.Invoke();
                FW_CinemachineCamera.instance.switchCamera("InitalCamera");
            }); 
            isComplete = true;
        }
    }

    void GameOver()
    {
        if (isGameOver)
        {
            print("GameOver");
            TargetCharacters.Clear();
            LeanTween.delayedCall(delayOnCompleteTask, () => {
                FW_CinemachineCamera.instance.switchCamera("3D Person View");
                foreach (FW_StundetThrowFoodEO item in Students)
                {
                    item.ShootEachOther = true;
                }
                LeanTween.delayedCall(delayOnCompleteTask-1f, () => { Perfect.Invoke(); });
            });
            
            
            isGameOver = false;
        }
    }
    void checkForTargets()
    {
        for (int i = 0; i < TargetCharacters.Count-1;)
        {
            if (TargetCharacters[i].isTargeted && TargetCharacters[i].isHit)
            {
                i++;
            }
            if (TargetCharacters[i].isTargeted && !TargetCharacters[i].isHit)
            {
                return;
            }
            else if(!TargetCharacters[i].isTargeted && !TargetCharacters[i].isHit)
            {
                TargetCharacters[i].isTargeted = true;
                return;
            }

        }
        if(TargetCharacters.Count>0)
            isGameOver = TargetCharacters.TrueForAll(TargetCharacters => TargetCharacters.isHit == true);
    }
}
