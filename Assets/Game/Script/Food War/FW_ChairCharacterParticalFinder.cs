using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FW_ChairCharacterParticalFinder : MonoBehaviour
{
    public FW_ChirCharacterSelector CCS;
    public FW_StundetThrowFoodEO FEO;
    public GameObject Kechup;
    
    public void HideKechup()
    {
        Kechup.SetActive(false);
    }

    public void GotHit()
    {
        if (!CCS.WorngTargetTrigger)
        {
            if(CCS.otherSelector) CCS.otherSelector.activeArrow = true;
        }
        
    }

    public void throwObjects()
    {
        FEO.ThrowTheFood();
    }

    public void AttackingState()
    {
        if (CCS.WorngTargetTrigger)
        {
            CCS.lookAtUs(CCS.Player, new Vector3(0.5f,0,0));
            LeanTween.delayedCall(FW_FlowManager.instance.failConditionDelay, () => {
                FW_FlowManager.instance.Failed.Invoke();
            });
           FEO.startShooting = true;
        }
    }
    public void ChangeTarget()
    {
        if (!CCS.WorngTargetTrigger)
        {
            LeanTween.delayedCall(0.15f, () => {
                if (FW_CinemachineCamera.instance.TargetNumber < 2) FW_CinemachineCamera.instance.TargetNumber += 1;
                FW_CinemachineCamera.instance.targetToNext();
            });
        }
       
    }
}
