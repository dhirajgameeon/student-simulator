using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FW_Food : MonoBehaviour
{
    public Camera cam;
    public Transform parant;
    public Vector3 offset;
    public float TrayYPos;

    public bool lastObject = false;

    private Vector3 mOffset;
    private float mZCoord;
    private bool mouseMoving = false;

    private void Start()
    {
        cam = Camera.main;
    }
    public void Update()
    {
        if (mouseMoving )
        {
            transform.position = GetMouseAsWorldPoint() + mOffset;
            transform.position = transform.position + offset;   
        }
    }
    void OnMouseDown()
    {
        mZCoord = cam.WorldToScreenPoint(gameObject.transform.position).z;
        mOffset = gameObject.transform.position - GetMouseAsWorldPoint();
        mouseMoving = true;
    }


    private Vector3 GetMouseAsWorldPoint()

    {

        // Pixel coordinates of mouse (x,y)

        Vector3 mousePoint = Input.mousePosition;


        // z coordinate of game object on screen

        mousePoint.z = mZCoord;


        // Convert it to world points

        return cam.ScreenToWorldPoint(mousePoint);

    }

    private void OnMouseUp()
    {
        mouseMoving = false;
        onPositionComplete();
    }

    void onPositionComplete()
    {
        transform.parent = parant;
        transform.position = transform.position - new Vector3(0, TrayYPos, 0);

        if (lastObject) LeanTween.delayedCall(0.15f, () => { FW_FlowManager.instance.isFoodCollected = true; });
    }
}
