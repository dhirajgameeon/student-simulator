using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FW_FoodAdder : MonoBehaviour
{
    public FW_FoodThrow FWF;
    public Camera cam;
    public LayerMask FoodMask;

    private void Update()
    {
        FoodRigidBodyAdder();
        RadarPositionChanger();
    }

    void FoodRigidBodyAdder()
    {
        if (!FWF.BallRigidbody)
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit, 100, FoodMask))
            {
                FWF.enabled = true;
                FWF.BallRigidbody = hit.transform.GetComponent<Rigidbody>();

            }
        }
    }

    void RadarPositionChanger()
    {
        if (FWF.BallRigidbody)
        {

        }
    }


}
