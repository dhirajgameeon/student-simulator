using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class FW_CinemachineCamera : MonoBehaviour
{
    public static FW_CinemachineCamera instance;
    public Animator anim;
    public GameObject PlayerCamera;

    public List<Transform> targets = new List<Transform>();

    public int TargetNumber;
    public List<float> targetsRotation = new List<float>();
    public List<Vector3> targetsPosition = new List<Vector3>();
    private void Start()
    {
        instance = this;
    }

    public void switchCamera(string state)
    {
        anim.Play(state);
    }


    public void targetToNext()
    {        
        LeanTween.rotate(PlayerCamera, new Vector3(10, targetsRotation[TargetNumber], 0), 0.5f);
        LeanTween.moveLocal(PlayerCamera, targetsPosition[TargetNumber], 0.15f);
    }

    public void changeCinamticLens(float f)
    {
        PlayerCamera.GetComponent<CinemachineVirtualCamera>().m_Lens.NearClipPlane = f;
    }
}
