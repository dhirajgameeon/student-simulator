using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FW_ChirCharacterSelector : MonoBehaviour
{
    public List<GameObject> Characters = new List<GameObject>();
    public GameObject Character;
    public FW_ChirCharacterSelector otherSelector;
    public FW_StundetThrowFoodEO FEO;
    public Transform Player;
    public GameObject TargetArrow;
    public ParticleSystem Emoji;
    public bool isPlay = true;

    public bool isTargeted = false;
    public bool isHit = false;
    public bool activeArrow = false;
    public bool WorngTargetTrigger = false;

    [Header("Throw Part")]
    public List<Rigidbody> ThrowingObjects = new List<Rigidbody>();

    void Start()
    {
        CharacterActivate();
    }
    private void OnEnable()
    {
    }
    private void Update()
    {
        if(FW_FlowManager.instance.isPlayerSeatOnBench) TargetChecks();
    }
    public void CharacterActivate()
    {
        Character = Characters[Random.Range(0, 3)];
        if (Character) Character.SetActive(true);
    }
    public void playAnimation()
    {
        if (isPlay) LeanTween.delayedCall(Random.Range(0, 1f), () => { Character.GetComponent<Animator>().Play("Laughing Left"); });
        if (!isPlay)
        {
            //lookAt(Player);
            LeanTween.delayedCall(Random.Range(0, 1f), () => { Character.GetComponent<Animator>().Play("Laughing Right"); });
        }
        LeanTween.delayedCall(Random.Range(0, 1f), () => { Emoji.Play(); });
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("food") && isTargeted)
        {
            activeArrow = false;
            isHit = true;
            FW_FlowManager.instance.PerfectTextUI.Play("popup");
            Character.GetComponent<FW_ChairCharacterParticalFinder>().Kechup.SetActive(true);
            Character.GetComponent<Animator>().Play("WipeFace");
            LeanTween.delayedCall(0.5f, () => { FW_FlowManager.instance.HitPlayers++; });
            Destroy(collision.gameObject);
        }
        WrongTarget(collision);
    }
    public void TargetChecks()
    {
        if(activeArrow)
        {
            TargetArrow.SetActive(true);
        }else
        {
            TargetArrow.SetActive(false);
        }

    }
    public void lookAtUs(Transform Target, Vector3 offset)
    {
        transform.LookAt(new Vector3(Target.position.x, transform.position.y, Target.position.z) + offset);
    }

    void WrongTarget(Collision collision)
    {
        if (collision.gameObject.CompareTag("food") && !isTargeted)
        {
            WorngTargetTrigger = true;
            Character.GetComponent<FW_ChairCharacterParticalFinder>().Kechup.SetActive(true);
            Character.GetComponent<Animator>().Play("WipeFace");
            Destroy(collision.gameObject);

        }
    }
}
