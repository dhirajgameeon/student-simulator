using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FW_Player : MonoBehaviour
{
    public NavMeshAgent agent;
    public Transform CounterTarget;
    public Transform ChairTarget;
    public GameObject TrayInHand;
    public GameObject TrayOnTable;
    public Animator anim;
    void Start()
    {
        agent.SetDestination(CounterTarget.position);        
    }

    private void Update()
    {
        if (FW_FlowManager.instance.isComplete)
        {
            LeanTween.delayedCall(FW_FlowManager.instance.delayAfterFoodCallection, () => { moveToChair(); });
            FW_FlowManager.instance.isComplete = false;
            FW_FlowManager.instance.isFoodCollected = false;
        }
    }

    void moveToChair()
    {
        agent.isStopped = false;
        agent.SetDestination(ChairTarget.position);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("counter"))
        {
            agent.isStopped = true;
            anim.Play("Idle With Tray");
            FW_CinemachineCamera.instance.switchCamera("Food Collection");
            //switch_camera
        }
        if (other.CompareTag("chair"))
        {
            agent.isStopped = true;
            RotateDirection(180, 0.5f);
            LeanTween.delayedCall(0.2f, () => { anim.Play("Sitting and Placing Tray"); });
            LeanTween.delayedCall(0.7f, () => { FW_FlowManager.instance.ProgressBar.gameObject.SetActive(true); });
            //         
            //switch_camera
        }
    }

    public void RotateDirection(float direction, float time)
    {
        LeanTween.rotate(this.gameObject, new Vector3(0, direction, 0), time);
    }
}
