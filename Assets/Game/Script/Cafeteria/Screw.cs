using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Screw : MonoBehaviour
{
    Rigidbody rb;
    public int ID;
    public bool screwOpend = false;
    public float Rotation;
    public float RotationTime;
    public Transform wrenchPosition;

    public UnityEvent onUnlokes;
    
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            ScrewAnimation();
        }
    }
    public void ScrewAnimation()
    {
        EvenManager.TriggerSFXOneShotPlayEvent(AudioID.Wrench);
        EvenManager.TriggerSFXOneShotPlayEvent(AudioID.WrenchLoop);
        LeanTween.rotateY(gameObject, 180.0f, RotationTime).setRepeat(99);
        LeanTween.move(this.gameObject, transform.position - new Vector3(0, 0.02f, 0), RotationTime);
        LeanTween.delayedCall(RotationTime, () => { /*rb.isKinematic = false; */ 
            LeanTween.move(this.gameObject, transform.position - new Vector3(0, 0.3f, 0), 1.5f).setOnComplete(() => { Destroy(this.gameObject); });
            LeanTween.rotateZ(gameObject, 40.0f, 0.1f);
            LeanTween.rotateX(gameObject, 30.0f, 0.1f);
        });
        LeanTween.delayedCall(3f, () => { screwOpend = true;
            EvenManager.TriggerSFXOneShotPlayEvent(AudioID.ScrewCompleted);
            CafateriaFlowManager.CFM.ScrewUnloked += 1;
            onUnlokes.Invoke();
            /*Destroy(this.gameObject, 1.5f); */
        }
        
        );
        LeanTween.delayedCall(3.8f, () => { if (ID == 2) CafateriaFlowManager.CFM.SwitchCamera("Below Table 2"); });
    }

    public void ScrewClicked()
    {
        /*Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if(Physics.Raycast)*/
    }

}
