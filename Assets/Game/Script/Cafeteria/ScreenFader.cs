using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenFader : MonoBehaviour
{
    public static ScreenFader sf;
    private void Awake()
    {
        sf = this;
    }
    public float fadeTime = 2;
    public void FadeStart(Image fader)
    {
        LeanTween.alpha(fader.rectTransform, 1f, fadeTime).setEase(LeanTweenType.linear).setOnComplete(() => {
            LeanTween.delayedCall(0.2f, () =>
            {
                FadeFinish(fader);
            }
            );
            }
        );
    }
    void FadeFinish(Image fader)
    {
        LeanTween.alpha(fader.rectTransform,0f, fadeTime).setEase(LeanTweenType.linear);
    }
}
