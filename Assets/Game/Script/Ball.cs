using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public Rigidbody ball;
    public Collider col;
    public Transform shootPoint;
    public float force;
    public bool isLaunched = false;

    public ParticleSystem Effect;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
      //  launch();
    }

    public void launch()
    {
        ball.AddForce(-transform.right * force, ForceMode.Impulse);
        if (Input.GetMouseButtonUp(0) && !isLaunched)
        {
            
            isLaunched = true;
        }
    }

    bool hit;

    private void OnCollisionEnter(Collision other)
    {
        switch (other.gameObject.tag)
        {
            case "Teacher":
                col.isTrigger = true;
                Effect.Play();
                EvenManager.TriggerSFXOneShotPlayEvent(AudioID.PaperHitTeacher);
                Destroy(this.gameObject, 0.5f);
                return;
            case "wind":
                launch();
                return;
        }


/*        if (other.gameObject.CompareTag("Teacher"))
        {
            
            Effect.Play();
            Destroy(this.gameObject, 0.5f);
        }
        else if(other.gameObject.CompareTag("wind"))
        {
            launch();

        }*/
        if (GameManager.instance.isThrownBall)
        {
            Invoke("SpawnNewBall", 0.5f);
            Destroy(this.gameObject, 0.5f);
            if (!hit) 
            {
                EvenManager.TriggerSFXOneShotPlayEvent(AudioID.PaperHitObject);
                FindObjectOfType<TeacherController>().LookDirection(0);
                Effect.Play();
                FindObjectOfType<FlowManager>().isMissed = true;
                hit = true;
            }
        }
    }

    void SpawnNewBall()
    {
        LeanTween.delayedCall(2.5f,() => { moveTeacher(); });
        
        GameManager.instance.SpawnBall();        
/*
        Invoke("moveTeacher", 0.5f);*/
    }
    void moveTeacher()
    {
        FindObjectOfType<TeacherController>().agent.SetDestination(FindObjectOfType<TeacherController>().PlayerPosition.position);
    }
}
