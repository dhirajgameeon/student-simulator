using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Face
{
    Happy,
    Angry,
    Idle
}
public class FaceExpression : MonoBehaviour
{
    public Face FaceType;
    public SkinnedMeshRenderer Eyes, Lips;
    public List<Texture> EyeSprite = new List<Texture>();
    public List<Texture> LipSprite = new List<Texture>();
    // Start is called before the first frame update
    void Start()
    {
        FaceType = Face.Idle;
    }

    // Update is called once per frame
    void Update()
    {
        FaceChanger();
    }
    public void FaceChanger()
    {
        switch (FaceType)
        {
            case Face.Happy:
                Eyes.material.mainTexture = EyeSprite[1];
                Lips.material.mainTexture = LipSprite[0];
                return;
            case Face.Angry:
                Eyes.material.mainTexture = EyeSprite[2];
                Lips.material.mainTexture = LipSprite[1];
                return;
            case Face.Idle:
                Eyes.material.mainTexture = EyeSprite[0];
                Lips.material.mainTexture = LipSprite[0];
                return;
        }
    }
}
