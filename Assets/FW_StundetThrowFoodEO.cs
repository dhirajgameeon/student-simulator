using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FW_StundetThrowFoodEO : MonoBehaviour
{
    public FW_ChirCharacterSelector FCCS;
    public Rigidbody rb;
    public Animator anim;
    public Transform ShootPosition;
    public bool startShooting;
    public bool ShootEachOther = false;
    public float force=10;
    public float throwTime = 1;
    private float Throw = 0;
    private void Update()
    {
        animatiorSelector();
        Shoot();
    }
    private void Start()
    {
        rb = FCCS.ThrowingObjects[Random.Range(0, 3)];
        Throw = Random.Range(throwTime / 2, throwTime * 2);
    }

    public void animatiorSelector()
    {
        if (FCCS.Character && !anim)
        {
            anim = FCCS.Character.GetComponent<Animator>();
        }
    }

    public void Shoot()
    {
        if (startShooting)
        {
            if (Throw > 0.5f) Throw -= Time.deltaTime * 10;
            if (Throw > 0 && Throw <= 0.5) Throw -= Time.deltaTime;
            if (Throw <= 0)
            {
                //shoot
                anim.Play("Throwing Food");
                
                Throw = throwTime;
            }
        }
        if (ShootEachOther)
        {
            if (Throw > 0) Throw -= Time.deltaTime;
            if (Throw <= 0)
            {
                //shoot
                anim.Play("Throwing Food");
                Throw = Random.Range(throwTime/2, throwTime*2);
            }
        }
    }

    public void ThrowTheFood()
    {
        if (rb)
        {
            Rigidbody FoodToThrow = Instantiate(rb, ShootPosition.position, Quaternion.identity);
            FoodToThrow.AddForce(ShootPosition.forward * force, ForceMode.Impulse);
            Destroy(FoodToThrow, 5);
        }
    }
}
