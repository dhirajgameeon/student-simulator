using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChairCharacterManager : MonoBehaviour
{
    public List<GameObject> Characters = new List<GameObject>();
    public GameObject Character;
    public Transform Player;
    public ParticleSystem Emoji;
    public bool isPlay = true;
    void Start()
    {
        CharacterActivate();
    }

    private void OnEnable()
    {        
        Player = GameObject.Find("Female Stud").transform;
    }
    public void CharacterActivate()
    {
        Character = Characters[Random.Range(0, 2)];
        if (Character) Character.SetActive(true);
    }
    public void playAnimation()
    {
        if(isPlay) LeanTween.delayedCall(Random.Range(0, 1f),()=> { Character.GetComponent<Animator>().Play("Laughing Left"); });
        if (!isPlay)
        {
            lookAt(Player);
            LeanTween.delayedCall(Random.Range(0, 1f), () => { Character.GetComponent<Animator>().Play("Laughing Right"); }); 
        }
        LeanTween.delayedCall(Random.Range(0, 1f),()=> { Emoji.Play(); });
    }
    void lookAt(Transform Target)
    {
        transform.LookAt(new Vector3(Target.position.x, transform.position.y, Target.position.z));
    }
}
