using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationTriggerForGirl : MonoBehaviour
{
    public CafeteriaGirlMovement CGM;

    public void TriggerNextStape()
    {
        CGM.FallDown();
    }

    public void RemoveTray()
    {
        CGM.removeTray();
    }

    public void Rotate()
    {
        CGM.RotateDirection(0, 0.1f);
    }

    public void CharacterFallen()
    {
        FindObjectOfType<CafateriaFlowManager>().CharacterFallen = true;
    }
}
