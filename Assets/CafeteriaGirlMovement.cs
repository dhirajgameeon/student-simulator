using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CafeteriaGirlMovement : MonoBehaviour
{
    public NavMeshAgent agent;
    public Animator anim;
    public List<Rigidbody> Weights = new List<Rigidbody>();

    private float turnSmoothVelocity;
    public float rotationSmooth = 0.1f;

    public Transform target;

    public GameObject TrayInHand;
    public GameObject TrayOnTable;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        move();
    }

    void move()
    {
        if (agent.velocity.magnitude > 0f)
        {
            float targetAngle = Mathf.Atan2(agent.velocity.x, agent.velocity.z) * Mathf.Rad2Deg;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, rotationSmooth);
            transform.rotation = Quaternion.Euler(0, angle, 0);
        }
    }

    public void removeTray()
    {
        EvenManager.TriggerSFXOneShotPlayEvent(AudioID.PlateOnTable);
        TrayInHand.SetActive(false);
        TrayOnTable.SetActive(true);
    }
    public void FallDown()
    {
        anim.SetBool("fall", true);
        EvenManager.TriggerSFXOneShotPlayEvent(AudioID.FemaleScream);
        EvenManager.TriggerSFXOneShotPlayEvent(AudioID.TabelFall);
        foreach (Rigidbody rb in Weights.ToArray())
        {
            rb.isKinematic = false;
        }
        LeanTween.delayedCall(0.01f, () => {
            
        });
    }
    public void RotateDirection(float direction, float time)
    {
        LeanTween.rotate(this.gameObject, new Vector3(0, direction, 0), time);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("StudentStop"))
        {

            agent.isStopped = true;            
            anim.Play("sitting");
        }
    }
}
