using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class CafateriaFlowManager : MonoBehaviour
{
    public static CafateriaFlowManager CFM;

    public List<GameObject> Character = new List<GameObject>();
    public List <ChairCharacterManager> CCM = new List<ChairCharacterManager>();
    public List<Screw> Screws = new List<Screw>();
    public CafeteriaGirlMovement CGM;
    public bool ScrewOpend = false;
    public GameObject WrenchIconButton;

    public Slider ProgressBar;
    public float ScrewUnloked = 0;
    public float upSpeed = 10;
    private float currentProgress;

    public bool CharacterFallen = false;

    public Image FadeScreen;
    public Animator Cinemachine;

    public Camera Cam;
    public LayerMask Screw;

    public GameObject Wrench;

    public UnityEvent OnScrewOpen;
    public UnityEvent OnComplete1stHalf;
    public UnityEvent OnComplete;
    // Start is called before the first frame update
    void Start()
    {
        EvenManager.TriggerSFXOneShotPlayEvent(AudioID.BGM);
        CFM = this;

        foreach (GameObject item in Character.ToArray())
        {
            item.SetActive(false);
        }

/*        foreach (Screw item in Screws.ToArray())
        {
            item.screwOpend = true;
        }*/

    }

    // Update is called once per frame
    void Update()
    {
        progress();
        if (Input.GetMouseButtonDown(0))
        {
            RaycastReader();
        }
        if(Screws.Count > 0) checkIfAllScrewOpend();
        CharacterFallenState();
    }

    public void CharacterFallenState()
    {
        if (CharacterFallen)
        {
            foreach (ChairCharacterManager item in CCM.ToArray())
            {
                item.playAnimation();
            }
            LeanTween.delayedCall(1f, () => { 
                OnComplete.Invoke();
                EvenManager.TriggerSFXOneShotPlayEvent(AudioID.GameWin);
                EvenManager.TriggerSFXOneShotPlayEvent(AudioID.ChildrenLaughing);
            });
            CharacterFallen = false;
        }
    }

    void progress()
    {
        if (currentProgress < ScrewUnloked)
        {
            currentProgress += upSpeed * Time.deltaTime;
            if (currentProgress >= ScrewUnloked)
                currentProgress = ScrewUnloked;
        }
        ProgressBar.value = currentProgress;
    }
    public void FadeCameraChange(string camera)
    {
        ScreenFader.sf.FadeStart(FadeScreen);
        LeanTween.delayedCall(0.1f, () => { SwitchCamera(camera); });
    }

    public void RaycastReader()
    {
        Ray ray = Cam.ScreenPointToRay(Input.mousePosition);
        if(Physics.Raycast(ray, out RaycastHit hit, 100, Screw))
        {
            hit.transform.GetComponent<Screw>().ScrewAnimation();
            PlasMover(hit.transform.GetComponent<Screw>().wrenchPosition, 0.15f);
        }
    }

    public void checkIfAllScrewOpend()
    {
        ScrewOpend = Screws.TrueForAll(Screws => Screws.GetComponent<Screw>().screwOpend == true);
        if (ScrewOpend)
        {
            OnScrewOpen.Invoke();
            EvenManager.TriggerSFXOneShotPlayEvent(AudioID.GameLoose);
            Wrench.SetActive(false);
            WrenchIconButton.SetActive(false);
            Screws.Clear();
            ScrewOpend = false;
        }
    }

    public void ScreenSwitch(float delay)
    {
        LeanTween.delayedCall(delay, () => {
            
            FadeCameraChange("InitalCamera");
            CGM.gameObject.SetActive(true);
            LeanTween.delayedCall(0.1f, () => { 
                CGM.agent.SetDestination(CGM.target.position);
                EvenManager.TriggerSFXOneShotPlayEvent(AudioID.StudentHumming);
            });
            LeanTween.delayedCall(0.1f, () => {
                OnComplete1stHalf.Invoke();
                foreach (GameObject item in Character.ToArray())
                {
                    item.SetActive(true);
                }
            });


        });

        
        
    }

    public void setActiveButton()
    {
        if (WrenchIconButton.activeSelf) WrenchIconButton.SetActive(false);
        if (!WrenchIconButton.activeSelf) LeanTween.delayedCall(0.1f, () => { WrenchIconButton.SetActive(true); });
    }
    public void SwitchCamera(string cameraAnimationName)
    {
        Cinemachine.Play(cameraAnimationName);
    }
    public void PlasMover(Transform pos, float t)
    {
        //LeanTween.move(Wrench, pos.position, t);
        Wrench.transform.position = pos.position;
        Wrench.transform.GetChild(1).GetComponent<ParticleSystem>().Play();
        Wrench.transform.rotation = pos.rotation;
        Wrench.GetComponent<Animator>().Play("Open");
    }
}
